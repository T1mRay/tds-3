// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_2_1_4 : ModuleRules
{
	public TDS_2_1_4(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
